/*








 //#######################################################################################
 //Class to insert MidiTimecode into a given MidiBuffer, suitable for block
 //based audio callbacks. The class can act on position jumps e.g. "Loops" by sending a
 //positioning message and supressing the following quarterframe messages up to the jump.
 //Timecode formats must NOT be dropframe formats (but might be easy to add)!
 //NOTE: No ballistik came in to play so I wouldn't recommend using it to drive a mechanical
 //tape deck!!!
 //
 //solar3d-software, April- 10- 2012
 //#######################################################################################








 */
#pragma once

#include "Includes.h"

class JK_MidiTimecode
{
   public:
    void
    setPositionJumpThreshold(const double ms)
    {
        posChangeThreshold = ms / 1000.0;
    }
    double
    getPositionJumpThreshold()
    {
        return posChangeThreshold;
    }
    void
    setFollowSongPosition(const bool shouldFollow)
    {
        followSongPosition = shouldFollow;
    }
    bool
    getFollowSongPosition()
    {
        return followSongPosition;
    }
    void
    setOffset(const int offset)
    {
        ppqOffset = offset;
    }
    int
    getOffset()
    {
        return ppqOffset;
    }

    void generateMidiTimecode(
        const AudioPlayHead::PositionInfo& positionInfo, MidiBuffer* midiBuffer, int bufferSize, double sampleRate, int smpteFrameRate);


   private:
    int64
    roundToInt64(double val) noexcept
    {
        return (val - floor(val) >= 0.5) ? (int64)(ceil(val)) : (int64)(floor(val));
    }

    int    nextTimeNibble     = 0;
    bool   wasPlaying         = false;
    double syncPpqPosition    = -999.0;
    double posChangeThreshold = 0.001;
    double ppqToStartSyncAt   = 0.0;
    bool   followSongPosition = true;
    uint8  syncFlag           = 0;
    int    ppqOffset          = 0;

    static const int gCycleEnd = 1;

    void getSmpteTime(double frameRate, long double absSecs, int& hours, int& mins, int& secs, int& frames);

    void sendFullFrameMessage(double                         ppqPosition,
                              double                         bmp,
                              double                         frameRateInDouble,
                              int                            posInBuffer,
                              MidiBuffer*                    buffer,
                              MidiMessage::SmpteTimecodeType smpteType);

    void sendQuarterFrameMessage(double                         ppqPosition,
                                 double                         bmp,
                                 double                         frameRateInDouble,
                                 int                            posInBuffer,
                                 MidiMessage::SmpteTimecodeType mtcType,
                                 MidiBuffer*                    buffer);

    bool positionJumped(double lastPosInPPQ, double currentPosInPPQ, double sampleRate, double ppqPerSample);

    double
    getNearestSixteenthInPPQ(double ppqPosition)
    {
        return ceil(ppqPosition * 4.0) / 4.0;
    }

    JUCE_LEAK_DETECTOR(JK_MidiTimecode);
};
