/*

 //#######################################################################################
 //Class to insert MidiTimecode into a given MidiBuffer, suitable for block
 //based audio callbacks. The class can act on position jumps e.g. "Loops" by sending a
 //positioning message and supressing the following quarterframe messages up to the jump.
 //Timecode formats must NOT be dropframe formats (but might be easy to add)!
 //NOTE: No ballistik came in to play so I wouldn't recommend using it to drive a mechanical
 //tape deck!!!
 //
 //solar3d-software, April- 10- 2012
 //#######################################################################################

*/
#include "JK_MidiTimecode.h"

void
JK_MidiTimecode::generateMidiTimecode(
    const AudioPlayHead::PositionInfo& positionInfo, MidiBuffer* midiBuffer, int bufferSize, double sampleRate, int smpteFrameRate)
{
    //###################################Some explanation about musical tempo #################
    // A Time Signature, is two numbers, one on top of the other. The numerator describes the  #
    // number of Beats in a Bar, while the denominator describes of what note value a Beat is. #
    // So 4/4 would be four quarter-notes per Bar, while 4/2 would be four half-notes per Bar, #
    // 4/8 would be four eighth-notes per Bar, and 2/4 would be two quarter-notes per Bar.     #
    //#########################################################################################

    if (midiBuffer != nullptr)
    {
        const double bpm = *positionInfo.getBpm();

        MidiMessage::SmpteTimecodeType mtcType;
        double                         frameRate;

        switch (smpteFrameRate)
        {
        // drop frames aren't supported
        case 0:
            frameRate = 24.0;
            mtcType   = MidiMessage::SmpteTimecodeType::fps24;
            break;
        case 1:
            frameRate = 25.0;
            mtcType   = MidiMessage::SmpteTimecodeType::fps25;
            break;
        case 3:
            frameRate = 30.00;
            mtcType   = MidiMessage::SmpteTimecodeType::fps30;
            break;
        default: frameRate = 24.0; mtcType = MidiMessage::SmpteTimecodeType::fps24;  // unknown or drop frame TC
        }

        // PPQ value of one sample
        const double ppqPerSample = (bpm / 60.0) / sampleRate;

        const auto ppqPosition = *positionInfo.getPpqPosition();

        // PPQ offset to compensate Midi interface latency
        double hostPpqPosition = ppqPosition + ppqOffset * ppqPerSample;
        ;

        if (positionInfo.getIsPlaying() || positionInfo.getIsRecording())
        {
            if (!wasPlaying)
            {
                // set the point where to start injecting
                ppqToStartSyncAt = getNearestSixteenthInPPQ(hostPpqPosition);

                // Special case: Master is set to always start playback from the previous start position...
                if (positionJumped(syncPpqPosition, hostPpqPosition, sampleRate, ppqPerSample))
                    sendFullFrameMessage(hostPpqPosition, bpm, frameRate, 0, midiBuffer, mtcType);
            }
            else
            {
                // Position jump (loop or manually position change while playing)
                if (positionJumped(syncPpqPosition, hostPpqPosition, sampleRate, ppqPerSample))
                {
                    // set the point where to start injecting
                    ppqToStartSyncAt = getNearestSixteenthInPPQ(hostPpqPosition);

                    // User has changed position manually while playing
                    if (syncFlag == 0)
                        sendFullFrameMessage(hostPpqPosition, bpm, frameRate, 0, midiBuffer, mtcType);
                    else
                    {
                        if (followSongPosition) sendFullFrameMessage(hostPpqPosition, bpm, frameRate, 0, midiBuffer, mtcType);

                        syncFlag = 0;
                    }
                }
            }

            for (int posInBuffer = 0; posInBuffer < bufferSize; ++posInBuffer)
            {
                syncPpqPosition = hostPpqPosition + (posInBuffer * ppqPerSample);

                const int   quarterMsgDistanceInSamples = roundToInt(sampleRate / (4.0 * frameRate));
                const int64 hostSamplePos               = roundToInt64((hostPpqPosition * (60.0 / bpm)) * sampleRate);
                const int64 syncSamplePos               = hostSamplePos + posInBuffer;

                // Some hosts like Cubase come up with a wacky ppqPosition.
                // That could break the timing! Best is to "wait"
                // here for the right ppqPosition to jump on.
                if (syncPpqPosition >= ppqToStartSyncAt)
                {
                    // Cycle mode on
                    auto loopPoints = *positionInfo.getLoopPoints();
                    if (positionInfo.getIsLooping() && loopPoints.ppqStart != loopPoints.ppqEnd)
                    {
                        const double ppqToCycleEnd     = fabs(loopPoints.ppqEnd - syncPpqPosition);
                        const int64  samplesToCycleEnd = roundToInt64(ppqToCycleEnd * (60.0 / bpm) * sampleRate);

                        // We can't stop a MTC slave using a Midi message. He will stop automatically if
                        // quarter messages go missing! So let's suppress any further
                        // quarter messages from now on up to cycle- end position.
                        if (samplesToCycleEnd <= 30 * quarterMsgDistanceInSamples)  // For fine tuning tweak here
                            syncFlag = gCycleEnd;
                    }

                    if (syncFlag == 0 || followSongPosition == false)
                        if (syncSamplePos % quarterMsgDistanceInSamples == 0)
                            sendQuarterFrameMessage(syncPpqPosition, bpm, frameRate, posInBuffer, mtcType, midiBuffer);
                }
            }

            wasPlaying = true;
        }
        else
        {
            // Send positioning message if the user has stopped or if he changed the playhead position
            // manually in stop mode! This will also initially cue slaves after loading plugin instance.
            if (wasPlaying || positionJumped(syncPpqPosition, hostPpqPosition, sampleRate, ppqPerSample))
                sendFullFrameMessage(hostPpqPosition, bpm, frameRate, 0, midiBuffer, mtcType);

            syncPpqPosition = hostPpqPosition;

            wasPlaying = false;
        }
    }
}

bool
JK_MidiTimecode::positionJumped(const double lastPosInPPQ, const double currentPosInPPQ, const double sampleRate, const double ppqPerSample)
{
    // This returns true if the user has changed the playhead position manually or if
    // a jump has occured! The comperator's default threshold is lastPosInPPQ +- 10ms.
    if (currentPosInPPQ < lastPosInPPQ - ((posChangeThreshold * sampleRate) * ppqPerSample) ||
        currentPosInPPQ > lastPosInPPQ + ((posChangeThreshold * sampleRate) * ppqPerSample))
        return true;

    return false;
}

void
JK_MidiTimecode::getSmpteTime(double frameRate, long double absSecs, int& hours, int& mins, int& secs, int& frames)
{
    hours  = static_cast<int>(absSecs / (60.0 * 60.0));
    mins   = (static_cast<int>(absSecs / 60.0)) % 60;
    secs   = static_cast<int>(absSecs) % 60;
    frames = roundToInt(absSecs * frameRate) % static_cast<int>(frameRate);
}

void
JK_MidiTimecode::sendFullFrameMessage(const double                   ppqPosition,
                                      const double                   bmp,
                                      const double                   frameRate,
                                      const int                      posInBuffer,
                                      MidiBuffer*                    buffer,
                                      MidiMessage::SmpteTimecodeType smpteType)
{
    const long double absSecs = fabs((ppqPosition * 60.0) / bmp);

    int hours, mins, secs, frames;
    getSmpteTime(frameRate, absSecs, hours, mins, secs, frames);

    MidiMessage fullFrameMessage(MidiMessage::fullFrame(hours, mins, secs, frames, smpteType));
    fullFrameMessage.setTimeStamp(static_cast<double>(posInBuffer));

    buffer->addEvent(fullFrameMessage, posInBuffer);

    nextTimeNibble = 0;
}

void
JK_MidiTimecode::sendQuarterFrameMessage(
    double ppqPosition, double bmp, double frameRate, int posInBuffer, MidiMessage::SmpteTimecodeType mtcType, MidiBuffer* buffer)
{
    int type;
    switch (mtcType)
    {
    case MidiMessage::SmpteTimecodeType::fps25: type = 1; break;
    case MidiMessage::SmpteTimecodeType::fps30: type = 3; break;
    default: type = 0; break;  // 24.0fps
    }

    const long double absSecs = fabs((ppqPosition * 60.0) / bmp);

    int hours, mins, secs, frames;
    getSmpteTime(frameRate, absSecs, hours, mins, secs, frames);

    int data = 0;

    switch (nextTimeNibble)
    {
    case 0: data = frames & 0x0f; break;
    case 1: data = (frames & 0xf0) >> 4; break;
    case 2: data = secs & 0x0f; break;
    case 3: data = (secs & 0xf0) >> 4; break;
    case 4: data = mins & 0x0f; break;
    case 5: data = (mins & 0xf0) >> 4; break;
    case 6: data = hours & 0x0f; break;
    case 7: data = (hours & 0x10) >> 4 | type; break;
    default: return;
    }

    MidiMessage quarterFrameMessage(MidiMessage::quarterFrame(nextTimeNibble, data));

    quarterFrameMessage.setTimeStamp(static_cast<double>(posInBuffer));

    buffer->addEvent(quarterFrameMessage, posInBuffer);

    ++nextTimeNibble;

    nextTimeNibble &= 0x7;
}
